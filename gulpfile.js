var fs = require('fs'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    webpack = require('webpack'),
    uglify = require('uglify-js');

var ENTRY       = './index.js',
    FILE        = 'neptune.js',
    FILE_MIN    = 'neptune.min.js',
    FILE_MAP    = 'neptune.map',
    DIST        = './dist',
    t_JS     = DIST + '/' + FILE;


var webpackConfig = {
  entry: ENTRY,
  output: {
    library: 'neptune',
    path: DIST,
    filename: FILE
  },
  cache: true
};

var uglifyConfig = {
  outSourceMap: FILE_MAP,
  output: {
    comments: /@license/
  }
};

// create a single instance of the compiler to allow caching
var compiler = webpack(webpackConfig);

gulp.task('minify', function () {
  var oldCwd = process.cwd();
  process.chdir(DIST);

  try {
    var result = uglify.minify([FILE], uglifyConfig);

    fs.writeFileSync(FILE_MIN, result.code);
    fs.writeFileSync(FILE_MAP, result.map);

    gutil.log('Minified ' + FILE_MIN);
    gutil.log('Mapped ' + FILE_MAP);
  } catch(e) {
    throw e;
  } finally {
    process.chdir(oldCwd);
  }
});

gulp.task('watch', function () {
  gulp.watch(['index.js', 'answers/**/*.js']);
});

gulp.task('default', ['minify']);