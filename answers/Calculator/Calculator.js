function run(query){
	var math = require('mathjs');
	try {
		var result = math.eval(query);
		var notMath = /^\d*$/ig;
		notMath = notMath.test(query);
		//console.log(notMath);
		if(result != null && !notMath){
			return { result: result, name: 'calculator'  };
		}
		return null;
	}
	catch(err) {
	    return null;
	}
}

exports.run = run;