var answer = require('./answers/load');

function start(query){
	var results = [];

	for (var prop in answer) {
		if (answer.hasOwnProperty(prop)) {
			results.push(answer[prop].run(query));
		}
	}

	results =results.filter(function(e){return e;});
	return {results: results};
}

exports.start = start;